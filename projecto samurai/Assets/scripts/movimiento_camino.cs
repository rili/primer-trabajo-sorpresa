using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movimiento_Camino : MonoBehaviour
{
    public float speed = 1f;

    private void Update()
    {
        transform.position += Vector3.right * speed * Time.deltaTime;
    }
}