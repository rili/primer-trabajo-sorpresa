 using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlJuego : MonoBehaviour
{
    public Transform jugador;
    public Transform camara;
    public GameObject flecha;
    public float velocidadAcercamiento ;
    public float tiempoMaximoMouseApretado = 5f;
    public float intensidadScreenShakeMaxima = 0.5f;
    public float velocidadRalentizacion = 0.5f;
    public float tiempoVolverAlNormal = 1f;
    public static float cantEne;

    private bool mouseApretado = false;
    private float tiempoMouseApretado = 0f;
    private Vector3 posicionInicialCamara;
    private Vector3 posicionInicialFlecha;
    private float intensidadScreenShake = 0f;
    private float tiempoRalentizacion = 1f;


    // Start is called before the first frame update
    void Start()
    {
        cantEne = 0;
        posicionInicialCamara = camara.position;
        posicionInicialFlecha = flecha.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(1) && tiempoMouseApretado < tiempoMaximoMouseApretado)
        {
            mouseApretado = true;
            tiempoMouseApretado += Time.deltaTime;

            if (mouseApretado== true)
            {
                // Acercar la c�mara al jugador
                Vector3 direccion = jugador.position - camara.position;
                camara.position += direccion * velocidadAcercamiento;

                // Screen shake
                intensidadScreenShake = Mathf.Lerp(0f, intensidadScreenShakeMaxima, tiempoMouseApretado / tiempoMaximoMouseApretado);
                camara.position += Random.insideUnitSphere * intensidadScreenShake;

                // Ralentizar el tiempo
                Time.timeScale = Mathf.Lerp(1f, 0f, tiempoMouseApretado / tiempoMaximoMouseApretado);

                // Evitar que la ralentizaci�n del tiempo afecte a la flecha
                flecha.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation;

            }


        }
        else
        {
            mouseApretado = false;
            if (mouseApretado == false)
            {
                tiempoMouseApretado = 0f;
                // Volver la c�mara y el tiempo a la normalidad
                camara.position = Vector3.Lerp(camara.position, posicionInicialCamara, Time.deltaTime / tiempoVolverAlNormal);
                Time.timeScale = Mathf.Lerp(Time.timeScale, 1f, Time.deltaTime / tiempoVolverAlNormal);

                // Permitir que la ralentizaci�n del tiempo afecte a otros objetos
                flecha.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;

            }





        }
    }
}
