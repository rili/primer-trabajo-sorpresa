using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlJugador : MonoBehaviour
{
    public float speed = 5f; 
    private float horizontalMove; 
    private float verticalMove;
    private Rigidbody rb;
    public BoxCollider col;
    public LayerMask capaPiso;
    public float magnitudSalto;
    private int saltos = 2;
    public int velocidadgiro;
    private bool girando = true;
    public GameObject kunai;
    public GameObject flecha;
    public GameObject yo;
    public GameObject kunaiejemplo;
    public GameObject pantallagameover;
    GameObject bal;
    public int X;
    public int Y;
    public int Z;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        col = GetComponent<BoxCollider>();
    }

    // Update is called once per frame
    private void Update()
    {
        girar();
        int fullsaltos = 2;

        horizontalMove = Input.GetAxis("Horizontal");
        verticalMove = Input.GetAxis("Vertical");
        Vector3 movement = new Vector3((horizontalMove * -1), verticalMove, 0f);
        transform.position += movement * speed * Time.deltaTime;

        
        if (Input.GetMouseButtonDown(0))
        {
            bal = Instantiate(kunai, (yo.transform.position + new Vector3(-1, 0, 0)), kunaiejemplo.transform.rotation);
            Destroy(bal, 2);
        }

    }



    private void girar()
    {
        if (girando == true)
        {
            transform.Rotate(Vector3.forward * Time.deltaTime * velocidadgiro);
        }
        else
        {
            transform.rotation = Quaternion.identity;
        }
    }
    private void perder()
    {
        GestorDeAudio.instancia.ReproducirSonido("hurt");
        pantallagameover.SetActive(true);
        Time.timeScale = 0;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("enemigo") == true)
        {

            Invoke("perder", 0.5f);
        }
    }
        
}
    
