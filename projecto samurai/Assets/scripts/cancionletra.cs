using UnityEngine;
using UnityEngine.UI;

public class CancionLetra : MonoBehaviour
{
    public AudioClip music;
    public Text lyricsText;
    public float delayBetweenLyrics = 0.5f;

    private string[] lyrics = new string[]
{
    "Ever on and on",
    "I continue circling",
    "With nothing but my hate",
    "A carousel of agony",
    "Till slowly i forget and my heart starts",
    "vanishing and suddenly i see",
    "That i cant break free im",
    "slipping through the cracks of a dark",
    "eternity with nothing but my pain",
    "and the paralyzing agony",
    "To tell me who i am",
    "who i was",
    "uncertainty enveloping my mind",
    "till i cant",
    "break free, and",
    "maybe its a dream",
    "maybe nothing else is real",
    "but it wouldnt mean a thing",
    "if i told you how i feel",
    "so im tired of all the pain",
    "of the misery inside",
    "and i wish that i could live ",
    "feeling nothing but the night",
    "you can tell me what to say",
    "you can tell me where to go",
    "but i doubt that i would care",
    "and my heart would never know",
    "if i make another move",
    "there�ll be no more turning back",
    "because everything will change",
    "and it all will fade to black",
    "will tomorrow ever come?",
    "will i make it through the night?",
    "will there ever be a place",
    "for the broken in the light?",
    "am i hurting? am i sad?",
    "should i stay or should i go?",
    "ive forgotten how to tell",
    "did i ever even know?",
    "can i take another step?",
    "ive done everything i can",
    "all the people that i see",
    "they will never understand",
    "if i find a way to change",
    "if i step into the light",
    "then ill never be the same",
    "and it all will fade to",
    "white",
    "Ever on and on",
    "I continue circling",
    "With nothing but my hate",
    "A carousel of agony",
    "Till slowly i forget and my heart starts",
    "vanishing and suddenly i see",
    "That i cant break free im",
    "slipping through the cracks of a dark",
    "eternity with nothing but my pain",
    "and the paralyzing agony",
    "To tell me who i am",
    "who i was",
    "uncertainty enveloping my mind",
    "till i cant",
    "break free, and",
    "maybe its a dream",
    "maybe nothing else is real",
    "but it wouldnt mean a thing",
    "if i told you how i feel",
    "so im tired of all the pain",
    "of the misery inside",
    "and i wish that i could live ",
    "feeling nothing but the night",
    "you can tell me what to say",
    "you can tell me where to go",
    "but i doubt that i would care",
    "and my heart would never know",
    "if i make another move",
    "there�ll be no more turning back",
    "because everything will change",
    "and it all will fade to black",
    "will tomorrow ever come?",
    "will i make it through the night?",
    "will there ever be a place",
    "for the broken in the light?",
    "am i hurting? am i sad?",
    "should i stay or should i go?",
    "ive forgotten how to tell",
    "did i ever even know?",
    "this time i will take a stand",
    "all the hatred in my eyes",
    "building up an evil plan",
    "standing lonely in the night",
    "with the darkness by my side",
    "looking deep inside myself",
    "and revealing only fright",
    "if i make another move",
    "if i take another step",
    "then it alll would fall apart",
    "thered be nothing of me left",
    "if im crying in the wind",
    "if im crying in the night",
    "will there ever be a way?",
    "will my heart return to white?",
    "can you tell me who you are?",
    "can you tell me where i am?",
    "ive forgotten how to see",
    "ive forgotten if i can",
    "if i opened up my eyes",
    "thered be no more going back",
    "cause id throw it all away",
    "and it all would fade to black",
    "so im back here once again",
    "so im back here once again",
    "can i ever make a change?",
    "will my heart begind to mend?",
    "would you love  me if i go?",
    "it feels like a heart attack",
    "but still everythings the same",
    "and it all just fades to ",
    "black"


};
    // ajustar cada linea de la cancion manualmente, solo esta a ritmo la primera 
    private float[] lyricTimes = new float[] 
    {
        1.15f, 2f, 3.5f, 5f, 7.7f, 9f, 11.1f, 13.0f, 14.7f, 16.4f, 18.2f, 19.7f, 21.4f,
    23.2f, 24.8f, 26.5f, 28.2f, 30.0f, 31.5f, 33.1f, 35.0f, 36.7f, 38.4f, 40.2f, 41.7f, 43.4f,
    45.1f, 46.7f, 48.4f, 50.2f, 51.7f, 53.4f, 55.0f, 57.0f, 58.7f, 60.5f, 62.0f, 63.7f, 65.4f,
    67.0f, 68.7f, 70.5f, 72.0f, 73.7f, 75.4f, 77.0f, 79.0f, 80.7f, 82.4f, 84.2f, 85.7f, 87.4f,
    89.1f, 90.7f, 92.4f, 94.2f, 95.7f, 97.4f, 99.0f, 101.0f, 102.7f, 104.4f, 106.2f, 107.7f, 109.4f,
    111.1f, 112.7f, 114.4f, 116.2f, 117.7f, 119.4f, 121.0f, 123.0f, 124.7f, 126.5f, 128.0f, 129.7f, 131.4f,
    133.0f, 134.7f, 136.5f, 138.0f, 139.7f, 141.4f, 143.0f, 145.0f, 146.7f, 148.4f, 150.2f, 151.7f, 153.4f,
    155.1f, 156.7f, 158.4f, 160.2f, 161.7f, 163.4f, 165.0f, 167.0f, 168.7f, 170.5f, 172.0f, 173.7f, 175.4f,
    177.0f, 178.7f, 180.5f, 182.0f, 183.7f, 185.4f, 187.0f, 189.0f, 190.7f, 192.4f, 194.2f, 195.7f, 197.4f
    };

    private int currentLyric = 0;
    private float timeSinceLastLyric = 0.0f;

    void Start()
    {
        GetComponent<AudioSource>().clip = music;
        GetComponent<AudioSource>().Play();
    }

    void Update()
    {
        timeSinceLastLyric += Time.deltaTime;

        if (timeSinceLastLyric >= delayBetweenLyrics)
        {
            timeSinceLastLyric = 0.0f;

            if (currentLyric < lyrics.Length && GetComponent<AudioSource>().time >= lyricTimes[currentLyric])
            {
                lyricsText.text = lyrics[currentLyric];
                currentLyric++;
            }
        }
    }
}
