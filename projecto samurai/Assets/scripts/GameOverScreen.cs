using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameOverScreen : MonoBehaviour

{
    public void Setup()
    {
        gameObject.SetActive(true);
        Cursor.lockState = CursorLockMode.None;
    }

    public void RestartButton()
    {
        ControlJuego1.puntuaciontotal = 0;
        SceneManager.LoadScene("nivel1");
    }

    public void ExitButton()
    {
        SceneManager.LoadScene("Exit");
    }
}
