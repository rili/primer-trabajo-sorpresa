using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dirkunai : MonoBehaviour
{
    public int velocidad;
    public GameObject yo;

    void Update()
    {
        transform.Translate(velocidad * Vector3.up * Time.deltaTime);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("enemigo") == true)
        {
            Destroy(yo, 0.0f);
        }
    }
}