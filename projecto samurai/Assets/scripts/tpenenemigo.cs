using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tpenenemigo : MonoBehaviour
{
    public Transform Jugador;
    public GameObject personaje;
    public Transform yo;

    void Start()
    {
        personaje = GameObject.Find("Jugador");
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("flecha") == true)
        {
            personaje.transform.position = yo.position;
        }

    }

}
