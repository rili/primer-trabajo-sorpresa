using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ControlFlecha : MonoBehaviour
{
    public GameObject jugador;
    public Transform playerTransform;
    public float velocidadRotacion = 50f;
    private Vector3 mousePos;
    public MeshRenderer meshRenderer;
    public BoxCollider boxCollider;

    void Update()
    {

        // Obtener la posici�n del mouse en el mundo
        Vector3 mousePos = Input.mousePosition;
        mousePos.z = Camera.main.transform.position.z - playerTransform.position.z;
        Vector3 worldMousePos = Camera.main.ScreenToWorldPoint(mousePos);

        // Calcular la direcci�n del mouse desde el objeto jugador
        Vector3 direction = worldMousePos - playerTransform.position;
        direction.z = 0f;




        // Rotar la flecha alrededor del objeto jugador en el eje z
        transform.position = (playerTransform.position + direction.normalized * 2f);
        transform.LookAt(playerTransform.position);
        transform.Rotate(Vector3.up, -90f);
        transform.Rotate(Vector3.forward, Time.deltaTime * velocidadRotacion);

        if (Input.GetMouseButton(1))
        {
            meshRenderer.enabled = true;
            boxCollider.enabled = true;
        }
        else
        {
            meshRenderer.enabled = false;
            boxCollider.enabled = false;
        }

    }
}
