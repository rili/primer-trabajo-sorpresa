using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;
using TMPro;


public class VidaPared : MonoBehaviour
{
    private int hp;
    private GameObject jugador;
    // Start is called before the first frame update
    void Start()
    {
        hp = 100;
        jugador = GameObject.Find("jugador");
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void recibirDaņo()
    {
        hp = hp - 25;

        if (hp <= 0)
        {
            this.desaparecer();
        }
    }

    private void desaparecer()
    {
        Destroy(gameObject);
    }

    //private void OnCollisionEnter (Collision collision)
    //{
    //    if (collision.gameObject.CompareTag("kunai"))
    //    {
    //        recibirDaņo();
    //    }
    //}


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("kunai") == true)
        {
            recibirDaņo();
        }
    }
}
