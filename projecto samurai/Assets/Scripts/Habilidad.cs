using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Habilidad : MonoBehaviour
{
    public float teleportDistance = 5f;
    public LayerMask enemyLayerMask;
    private bool rango = false;
    private bool isMouseDown = false;
    private Vector3 targetPosition;
    public Transform jugador;




    private void Update()
    {
        if (Input.GetMouseButtonDown(1))
        {
            isMouseDown = true;
        }

        if (Input.GetMouseButtonUp(1) && isMouseDown)
        {
            isMouseDown = false;

        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("enemigo") == true)
        {
            jugador.position = other.gameObject.transform.position;
        }

    }
}
